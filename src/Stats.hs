module Stats where


import Statistics.Sample

import Data.Vector (fromList)
import qualified Data.Map as M


analyseSample :: [Double] -> M.Map String Double
analyseSample vals = M.fromList [("Mean", (mean . fromList) vals)]

