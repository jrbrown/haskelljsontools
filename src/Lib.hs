module Lib 
  ( mainFunc
  ) where


import qualified Data.Map as M

import Data.List (intercalate)
import Stats (analyseSample)
import JsonObj (JsonVal)
import Transforms (mkJsonFunc)
import System.Environment (getArgs)
import FileIO (writeJson, readJson)


unaryFuncs :: M.Map String (JsonVal -> JsonVal)
unaryFuncs = M.fromList
  [ ("analyse_nums", mkJsonFunc analyseSample)
  , ("id", id) ]

validFunctions :: String
validFunctions = intercalate ", " $ (map fst . M.toList) unaryFuncs

parseArgs :: [String] -> (String, JsonVal -> JsonVal, String)
parseArgs [in_fp, func_str, out_fp] = (in_fp, func, out_fp)
  where func = case M.lookup func_str unaryFuncs of
                 Just x  -> x
                 Nothing -> error ("Not a valid function, valid functions are: " ++ validFunctions)
parseArgs _ = error "Program should be given 3 arguments, input file, function and output file"

applyFunc :: Maybe JsonVal -> (JsonVal -> JsonVal) -> JsonVal
applyFunc (Just x) f = f x
applyFunc Nothing _  = error "The file data could not be parsed as a valid JSON object"

mainFunc :: IO ()
mainFunc = do
  args <- getArgs
  let (in_fp, func, out_fp) = parseArgs args

  jObj <- readJson in_fp 
  let newJObj = applyFunc jObj func

  writeJson out_fp newJObj
  putStrLn "Success"

